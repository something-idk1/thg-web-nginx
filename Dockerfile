FROM php:7.4-fpm

ARG user
ARG uid

# Install system dependencies
RUN apt-get update && apt-get install -y \
    git \
    curl \
    libpng-dev \
    libonig-dev \
    libxml2-dev \
    zip \
    unzip

# Clear cache
RUN apt-get clean && rm -rf /var/lib/apt/lists/*

#  PHP extensions
RUN docker-php-ext-install pdo_mysql mbstring exif pcntl bcmath gd

#  Composer
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer
# Set COMPOSER_ALLOW_SUPERUSER=1
ENV COMPOSER_ALLOW_SUPERUSER 1



# Create system user to run Composer and Artisan Commands
RUN useradd -G www-data,root -u $uid -d /home/$user $user
RUN mkdir -p /home/$user/.composer && \
    chown -R $user:$user /home/$user


# Copy existing application directory contents
COPY . /var/www

# Set work dir
WORKDIR /var/www

# deppendencies for composer
RUN rm -rf vendor composer.lock
RUN composer install --no-interaction
RUN php artisan key:generate
